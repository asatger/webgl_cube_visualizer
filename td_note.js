function loadText(url) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, false);
    xhr.overrideMimeType("text/plain");
    xhr.send(null);
    if (xhr.status === 200)
        return xhr.responseText;
    else {
        return null;
    }
}

// variables globales du programme;
var canvas;
var gl; //contexte
var program; //shader program

// Attributes
var position_attr;
var color_attr;
var size_attr;
var FOV = 75;

// Uniform matrix
var pMatrix_unif;
var rMatrix_unif;
var tMatrix_unif;
var sMatrix_unif;

var buffer;

//Colors
var colors = [];
var opacity = 1.0;
var front, back, up, bottom, left, right = {};
var colorPicker;

//Face selected
var currentFace;

var pMatrix = mat4.create();
var rMatrix = mat4.identity(mat4.create());
var tMatrix = mat4.identity(mat4.create());
var sMatrix = mat4.identity(mat4.create());

var positions = [];

cube = {
    //initialisation translation
    x_translation: 0,
    y_translation: 0,
    z_translation: -4,

    //initialisation rotation
    x_rotation: 0,
    y_rotation: 0,
    z_rotation: 0,

    //initialisation scale
    scale: 0
};

function initContext() {
    canvas = document.getElementById('canvas');
    gl = canvas.getContext('webgl');
    if (!gl) {
        console.log('ERREUR : echec chargement du contexte');
        return;
    }
    gl.clearColor(0.2, 0.2, 0.2, 1.0);
    gl.enable(gl.DEPTH_TEST);

    mat4.perspective(pMatrix, FOV, canvas.width / canvas.height, 0.1, 100.0);

}

//Initialisation des shaders et du program
function initShaders() {

    positions = [
        /////// Face arrière //////
        //Triangle 1
        0.5, 0.5, -0.5,
        0.5, -0.5, -0.5,
        -0.5, 0.5, -0.5,

        //Triangle 2
        -0.5, -0.5, -0.5,
        0.5, -0.5, -0.5,
        -0.5, 0.5, -0.5,

        ////// Face avant ///////
        //Triangle 1
        -0.5, -0.5, 0.5,
        -0.5, 0.5, 0.5,
        0.5, 0.5, 0.5,

        //Triangle 2
        0.5, 0.5, 0.5,
        0.5, -0.5, 0.5,
        -0.5, -0.5, 0.5,

        ////// Face supérieure ///////
        //Triangle 1
        -0.5, 0.5, -0.5,
        -0.5, 0.5, 0.5,
        0.5, 0.5, 0.5,

        //Triangle 2
        0.5, 0.5, 0.5,
        0.5, 0.5, -0.5,
        -0.5, 0.5, -0.5,

        ////// Face inférieure //////
        //Triangle 1
        -0.5, -0.5, -0.5,
        -0.5, -0.5, 0.5,
        0.5, -0.5, 0.5,

        //Triangle 2
        0.5, -0.5, 0.5,
        0.5, -0.5, -0.5,
        -0.5, -0.5, -0.5,

        ////// Face droite //////
        //Triangle 1
        0.5, 0.5, -0.5,
        0.5, 0.5, 0.5,
        0.5, -0.5, 0.5,

        //Triangle 2
        0.5, -0.5, 0.5,
        0.5, -0.5, -0.5,
        0.5, 0.5, -0.5,

        ////// Face gauche //////
        //Triangle 1
        -0.5, 0.5, -0.5,
        -0.5, 0.5, 0.5,
        -0.5, -0.5, 0.5,

        //Triangle 2
        -0.5, -0.5, 0.5,
        -0.5, -0.5, -0.5,
        -0.5, 0.5, -0.5
    ];


    front = {
        color: [
            Math.random(),
            Math.random(),
            Math.random()
        ]
    };

    back = {
        color: [
            Math.random(),
            Math.random(),
            Math.random()
        ]
    };

    left = {
        color: [
            Math.random(),
            Math.random(),
            Math.random()
        ]
    };

    right = {
        color: [
            Math.random(),
            Math.random(),
            Math.random()
        ]
    };

    bottom = {
        color: [
            Math.random(),
            Math.random(),
            Math.random()
        ]
    };

    up = {
        color: [
            Math.random(),
            Math.random(),
            Math.random()
        ]
    };

    var fragmentSource = loadText('fragment.glsl');
    var vertexSource = loadText('vertex.glsl');

    var fragment = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragment, fragmentSource);
    gl.compileShader(fragment);

    var vertex = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertex, vertexSource);
    gl.compileShader(vertex);

    gl.getShaderParameter(fragment, gl.COMPILE_STATUS);
    gl.getShaderParameter(vertex, gl.COMPILE_STATUS);

    if (!gl.getShaderParameter(fragment, gl.COMPILE_STATUS)) {
        console.log(gl.getShaderInfoLog(fragment));
    }

    if (!gl.getShaderParameter(vertex, gl.COMPILE_STATUS)) {
        console.log(gl.getShaderInfoLog(vertex));
    }

    program = gl.createProgram();
    gl.attachShader(program, fragment);
    gl.attachShader(program, vertex);
    gl.linkProgram(program);
    if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
        console.log("Could not initialise shaders");
    }
    gl.useProgram(program);
}

//Evenements
function initEvents() {
    document.onkeydown = function (e) {
        switch (e.key) {
            case "ArrowLeft":
                mat4.translate(pMatrix, pMatrix, [-1, 0, 0]);
                break;

            case "ArrowUp":
                mat4.translate(pMatrix, pMatrix, [0, 1, 0]);
                break;

            case "ArrowRight":
                mat4.translate(pMatrix, pMatrix, [1, 0, 0]);
                break;

            case "ArrowDown":
                mat4.translate(pMatrix, pMatrix, [0, -1, 0]);
                break;

            case "z":
                mat4.rotateX(rMatrix, rMatrix, 2 * Math.PI / 60);
                break;

            case "s":
                mat4.rotateX(rMatrix, rMatrix, -2 * Math.PI / 60);
                break;

            case "q":
                mat4.rotateY(rMatrix, rMatrix, 2 * Math.PI / 60);
                break;

            case "d":
                mat4.rotateY(rMatrix, rMatrix, - 2 * Math.PI / 60);
                break;

            default:
                break;
        }
        refreshBuffers();
    };

    $(document).ready(function () {

        currentFace = $("#face_select")[0].value;

        $('#y_rotation').on('input', function () {

            mat4.rotateY(rMatrix, rMatrix, (cube.y_rotation - this.valueAsNumber));
            cube.y_rotation = this.valueAsNumber;
            refreshBuffers();
        });

        $('#x_rotation').on('input', function () {

            mat4.rotateX(rMatrix, rMatrix, (cube.x_rotation - this.valueAsNumber));
            cube.x_rotation = this.valueAsNumber;
            refreshBuffers();
        });

        $('#z_rotation').on('input', function () {

            mat4.rotateZ(rMatrix, rMatrix, (cube.z_rotation - this.valueAsNumber));
            cube.z_rotation = this.valueAsNumber;
            refreshBuffers();
        });

        $('#z_translation').on('input', function () {

            mat4.translate(tMatrix, tMatrix, [0, 0, (cube.z_translation - this.valueAsNumber)]);
            cube.z_translation = this.valueAsNumber;
            refreshBuffers();
        });

        $('#x_translation').on('input', function () {

            mat4.translate(tMatrix, tMatrix, [(cube.x_translation - this.valueAsNumber), 0, 0]);
            cube.x_translation = this.valueAsNumber;
            refreshBuffers();
        });

        $('#y_translation').on('input', function () {

            mat4.translate(tMatrix, tMatrix, [0, (cube.y_translation - this.valueAsNumber), 0]);
            cube.y_translation = this.valueAsNumber;
            refreshBuffers();
        });

        $('#zoom').on('input', function () {
            mat4.scale(sMatrix, sMatrix, [1 + (cube.scale - this.valueAsNumber), 1 + (cube.scale - this.valueAsNumber), 1 + (cube.scale - this.valueAsNumber)]);
            cube.scale = this.valueAsNumber;
            console.log(cube.scale);
            refreshBuffers();
        });

        $('#perspective').on('input', function () {
            FOV = this.valueAsNumber;
            mat4.perspective(pMatrix, FOV * Math.PI / 180, canvas.width / canvas.height, 0.1, 100.0);
            console.log(FOV);
            refreshBuffers();
        });

        $('#face_select').on('change', function () {
            console.log(this.value)

            currentFace = this.value;

            colorPicker.color.rgb = {
                r: getFaceObjectFromValue(currentFace).color[0] * 255,
                g: getFaceObjectFromValue(currentFace).color[1] * 255,
                b: getFaceObjectFromValue(currentFace).color[2] * 255
            };
        });

        colorPicker.on("color:change", function (color, changes) {
            console.log(color);
            getFaceObjectFromValue(currentFace).color = [
                color.rgb.r / 255,
                color.rgb.g / 255,
                color.rgb.b / 255
            ];
            //update colors array            
            initColors();

            refreshBuffers();
        });
    });

}

function getFaceObjectFromValue(val) {
    switch (val) {
        case "front":
            return front;

        case "back":
            return back;

        case "up":
            return up;

        case "bottom":
            return bottom;

        case "left":
            return left;

        case "right":
            return right;

        default:
            break;
    }
}

function initColors() {
    colors = [

        //back
        back.color[0], back.color[1], back.color[2], opacity,
        back.color[0], back.color[1], back.color[2], opacity,
        back.color[0], back.color[1], back.color[2], opacity,

        back.color[0], back.color[1], back.color[2], opacity,
        back.color[0], back.color[1], back.color[2], opacity,
        back.color[0], back.color[1], back.color[2], opacity,


        //front        
        front.color[0], front.color[1], front.color[2], opacity,
        front.color[0], front.color[1], front.color[2], opacity,
        front.color[0], front.color[1], front.color[2], opacity,

        front.color[0], front.color[1], front.color[2], opacity,
        front.color[0], front.color[1], front.color[2], opacity,
        front.color[0], front.color[1], front.color[2], opacity,

        //bottom
        bottom.color[0], bottom.color[1], bottom.color[2], opacity,
        bottom.color[0], bottom.color[1], bottom.color[2], opacity,
        bottom.color[0], bottom.color[1], bottom.color[2], opacity,

        bottom.color[0], bottom.color[1], bottom.color[2], opacity,
        bottom.color[0], bottom.color[1], bottom.color[2], opacity,
        bottom.color[0], bottom.color[1], bottom.color[2], opacity,

        //up
        up.color[0], up.color[1], up.color[2], opacity,
        up.color[0], up.color[1], up.color[2], opacity,
        up.color[0], up.color[1], up.color[2], opacity,

        up.color[0], up.color[1], up.color[2], opacity,
        up.color[0], up.color[1], up.color[2], opacity,
        up.color[0], up.color[1], up.color[2], opacity,

        //left
        left.color[0], left.color[1], left.color[2], opacity,
        left.color[0], left.color[1], left.color[2], opacity,
        left.color[0], left.color[1], left.color[2], opacity,

        left.color[0], left.color[1], left.color[2], opacity,
        left.color[0], left.color[1], left.color[2], opacity,
        left.color[0], left.color[1], left.color[2], opacity,

        //right
        right.color[0], right.color[1], right.color[2], opacity,
        right.color[0], right.color[1], right.color[2], opacity,
        right.color[0], right.color[1], right.color[2], opacity,

        right.color[0], right.color[1], right.color[2], opacity,
        right.color[0], right.color[1], right.color[2], opacity,
        right.color[0], right.color[1], right.color[2], opacity,
    ];
}

//Fonction initialisant les attributs pour l'affichage (position, taille et couleurs)
function initAttributes() {
    position_attr = gl.getAttribLocation(program, "position");
    color_attr = gl.getAttribLocation(program, "color");
    size_attr = 3;
}

//Fonction initialisant les attributs uniformes
function initUniform() {
    pMatrix_unif = gl.getUniformLocation(program, "perspective");
    rMatrix_unif = gl.getUniformLocation(program, "rotation");
    tMatrix_unif = gl.getUniformLocation(program, "translation");
    sMatrix_unif = gl.getUniformLocation(program, "scale");
}
//Initialisation des buffers
function initBuffers() {

    buffer = gl.createBuffer();
    bufferColor = gl.createBuffer();

    initAttributes();

    initUniform();

    gl.enableVertexAttribArray(position_attr);
    gl.enableVertexAttribArray(color_attr);

    //position initiale du cube
    mat4.translate(tMatrix, tMatrix, [cube.x_translation, cube.y_translation, cube.z_translation]);

    refreshBuffers();
}

//Mise a jour des buffers : necessaire car les coordonnees des points sont ajoutees a chaque clic
function refreshBuffers() {

    gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions), gl.STATIC_DRAW);
    gl.vertexAttribPointer(position_attr, size_attr, gl.FLOAT, true, 0, 0);

    gl.bindBuffer(gl.ARRAY_BUFFER, bufferColor);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(colors), gl.STATIC_DRAW);
    gl.vertexAttribPointer(color_attr, 4, gl.FLOAT, true, 0, 0);

    gl.uniformMatrix4fv(pMatrix_unif, false, pMatrix);
    gl.uniformMatrix4fv(rMatrix_unif, false, rMatrix);
    gl.uniformMatrix4fv(tMatrix_unif, false, tMatrix);
    gl.uniformMatrix4fv(sMatrix_unif, false, sMatrix);

    draw();
}

//Fonction permettant le dessin dans le canvas
function draw() {
    gl.clear(gl.COLOR_BUFFER_BIT);
    gl.drawArrays(gl.TRIANGLES, 0, positions.length / size_attr);
}

function createColorPicker() {
    colorPicker = new iro.ColorPicker("#color-picker-container", {
        width: 200,
        height: 300,
        color: { r: front.color[0] * 255, g: front.color[1] * 255, b: front.color[2] * 255 },
    });
}

function main() {

    initContext();
    initShaders();
    initColors();
    initBuffers();
    createColorPicker();
    initEvents();
}
